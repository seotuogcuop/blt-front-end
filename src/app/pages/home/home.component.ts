import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { SpinnerService } from 'src/app/commont/progress-spinner/progress-spinner.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    list_title: any[] = [];
    list_project: any[] = [];
    sidebar_content = false;
    constructor(private primengConfig: PrimeNGConfig, private spinner: SpinnerService) { }

    ngOnInit(): void {
        this.primengConfig.ripple = true;
        this.spinner.show();
        setTimeout(() => {
            this.spinner.hide();
        }, 1500);
        this.list_title = [
            {
                title: 'CÔNG VIỆC',
                images: 'https://static.upgo.vn/images/upinstantpage/792264180241615694465061',
                discription: 'Tuấn Sơn coi trọng kỷ luật, tác phong chuẩn chỉ, đòi hỏi các nhân sự chịu được áp lực cao, cống hiến và cháy hết mình trong công việc.'
            },
            {
                title: 'KHÁCH HÀNG',
                images: 'http://hungdungland.com/wp-content/uploads/2019/06/2.png',
                discription: 'Chính xác – Chuyên nghiệp – Chuyên sâu là tôn chỉ của Tuấn Sơn trong sứ mệnh phục vụ và chăm sóc khách hàng.'
            },
            {
                title: 'ĐỐI TÁC',
                images: 'http://hungdungland.com/wp-content/uploads/2019/06/3.png',
                discription: 'Trong kinh doanh,Tuấn Sơn Land đề cao mối quan hệ hợp tác, phát triển, tôn trọng đối tác trên tinh thần “Win-Win” – đôi bên cùng có lợi'
            },
            {
                title: 'ĐỒNG NGHIỆP',
                images: 'http://hungdungland.com/wp-content/uploads/2019/06/4.png',
                discription: 'Tại ngôi nhà chung Tuấn Sơn, các nhân sự được phát triển bình đẳng trên tinh thần đồng nghiệp đoàn kết, giúp đỡ, sẻ chia..'
            },
            {
                title: 'XÃ HỘI',
                images: 'http://hungdungland.com/wp-content/uploads/2019/06/icon05.png',
                discription: 'Tuấn Sơn thường xuyên tổ chức các hoạt động thiện nguyện phục vụ cộng đồng và coi đây là trách nhiệm, vinh dự của doanh nghiệp..'
            },
            {
                title: 'CHÍNH MÌNH',
                images: 'http://hungdungland.com/wp-content/uploads/2019/06/icon06.png',
                discription: 'Con người Tuấn Sơn đề cao tinh thần tự giác, nơi mỗi cá nhân đều chủ động học hỏi, không ngừng hoàn thiện và phát triển chính mình.'
            }
        ];
        this.list_project = [
            {
                name: 'sudico nam an khánh',
                images: 'https://static.upgo.vn/images/upinstantpage/932751444301615008663687'
            },
            {
                name: 'geleximco lê trọng tấn',
                images: 'https://static.upgo.vn/images/upinstantpage/629760396301615008661208'
            },
            {
                name: 'đất dịch vụ',
                images: 'https://static.upgo.vn/images/upinstantpage/429370153321615008658879'
            },
            {
                name: 'KDT Bảo Sơn Paradise',
                images: 'https://static.upgo.vn/images/upinstantpage/393621416031615010795971'
            },
            {
                name: 'Vinhome Hoa Phượng',
                images: 'https://static.upgo.vn/images/upinstantpage/940509963151615010792857'
            },
            {
                name: 'KDT Splendora',
                images: 'https://static.upgo.vn/images/upinstantpage/889866393151615010790212'
            }
        ]
    }

}
