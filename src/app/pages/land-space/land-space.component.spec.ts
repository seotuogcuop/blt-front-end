import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSpaceComponent } from './land-space.component';

describe('LandSpaceComponent', () => {
  let component: LandSpaceComponent;
  let fixture: ComponentFixture<LandSpaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandSpaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
