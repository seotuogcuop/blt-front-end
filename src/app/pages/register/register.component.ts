import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SpinnerService } from 'src/app/commont/progress-spinner/progress-spinner.service';
import { AccounntService } from 'src/app/services/accounnt.service';
import { ToastHelperService } from 'src/app/services/toastHelper.service';
import { UserService } from 'src/app/services/users.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    form: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private toastService: ToastHelperService,
        private spinner: SpinnerService
    ) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            display_name: [null, Validators.required],
            email: [null, [Validators.required, Validators.email]],
            password: [null, [Validators.required, Validators.minLength(6)]],
            confirm_password: [null, [Validators.required]],
        }, {
            validators: this.MustMatch('password', 'confirm_password')
        });

    }
    get f() { return this.form.controls; }
    onSubmit() {
        this.submitted = true;
        if (this.form.invalid) {
            return;
        }
        this.loading = true;
        this.spinner.show();
        this.userService.regiserAccount(this.form.value).subscribe((response: {error:{error_message: string}}) => {
            if (!response) {
                this.loading = false;
                this.spinner.hide();
                this.toastService.addErrorMessage('Email đã tồn tại!');
                return ;
            }
            if (response['error_code'] || response['error_message']) {
                this.loading = false;
                this.spinner.hide();
                return;
            }
            if (response) {
                console.log('resposne');
                this.loading = false;
                this.spinner.hide();
                this.toastService.addSuccessMessage('Đăng kí thành công !');
                this.router.navigateByUrl('login', {state: {value: this.form.value}});
            }
        })
        // this.userService.regiserAccount(this.form.value).subscribe({
        //     next: (response: any) => {
               
        //         this.loading = false;
        //         this.spinner.hide();
        //         this.toastService.addSuccessMessage('Đăng kí thành công !');
        //         this.router.navigateByUrl('login', { state: { value: this.form.value } });
        //     },
        //     error: error => {
        //         this.loading = false;
        //         this.spinner.hide();
        //         this.toastService.addErrorMessage('Đăng kí không thành công' + ' ' + error.error_message);
        //     }
        // });
    }

    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
    }

}
