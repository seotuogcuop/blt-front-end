import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccounntService } from 'src/app/services/accounnt.service';
import { ToastHelperService } from 'src/app/services/toastHelper.service';
import { UserService } from 'src/app/services/users.service';

@Component({
    selector: 'app-loggin',
    templateUrl: './loggin.component.html',
    styleUrls: ['./loggin.component.css']
})
export class LogginComponent implements OnInit {
    form: FormGroup;
    loading: boolean;
    submitted: boolean;
    _account: any;
    _password: any;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private messageSerice: ToastHelperService
    ) { 
        this.router.getCurrentNavigation().extras.state

    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            display_name: ['', Validators.required],
            password: ['', Validators.required]
        });
        this._account = '';
        this._password = '';
        const newAccount = history.state;
        if (newAccount) {
            if (newAccount.value) {
                this._account = (newAccount.value && newAccount.value.email) ? newAccount.value.email : null;
                console.log('this._account', this._account);
                this._password = (newAccount.value && newAccount.value.password) ? newAccount.value.password: null;
            }
        }
    }
    ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        
    }
    get f() { return this.form.controls; }
    onSubmit() {
        this.submitted = true;

        // reset alerts on submit

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        this.userService.login(this.f.display_name.value, this.f.password.value)
            .subscribe({
                next: (resposne: any) => {
                    // // get return url from query parameters or default to home page
                    // const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                    // console.log(returnUrl, '===============================>');
                    if (resposne.error_message || resposne.error_code) {
                        console.log('router dashbroad ========>')
                        this.messageSerice.addErrorMessage('Đăng nhập không thành công!' + resposne.error_message)
                        return ;
                    }
                    this.userService.setCurrentUser(resposne);
                    this.router.navigateByUrl('/dashboard');
                },
                error: error => {
                     this.loading = false;
                    this.router.navigateByUrl('dasbroad');

                }
            });
    }
    onChange ($event: any){
        if ($event) {
            console.log('$event', $event);
        }
    }

}
