import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  list_project: any[] = [];
  constructor() { }

  ngOnInit(): void {
    this.list_project = [
      {
        name : 'sudico nam an khánh',
        images :'https://static.upgo.vn/images/upinstantpage/932751444301615008663687'
      },
      {
        name : 'geleximco lê trọng tấn',
        images :'https://static.upgo.vn/images/upinstantpage/629760396301615008661208'
      },
      {
        name : 'đất dịch vụ',
        images :'https://static.upgo.vn/images/upinstantpage/429370153321615008658879'
      },
      {
        name : 'KDT Bảo Sơn Paradise',
        images :'https://static.upgo.vn/images/upinstantpage/393621416031615010795971'
      },
      {
        name : 'Vinhome Hoa Phượng',
        images :'https://static.upgo.vn/images/upinstantpage/940509963151615010792857'
      },
      {
        name : 'KDT Splendora',
        images :'https://static.upgo.vn/images/upinstantpage/889866393151615010790212'
      }
    ]
  }

}
