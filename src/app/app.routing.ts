import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './comon/auth.guard';
import { AccountLayoutComponent } from './layouts/account-layout/account-layout.component';
import { LogginComponent } from './pages/loggin/loggin.component';
import { RegisterComponent } from './pages/register/register.component';

// const usersModule = () => import('./users/users.module').then(x => x.UsersModule);
const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
     {
         path: 'login', component: LogginComponent,
     },
     {
         path: 'register', component: RegisterComponent
     },
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule',
            }
        ],  }, {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
            }
        ],
    }, {
        path: '**',
        redirectTo: 'login'
    }, {
        path: '', component : AccountLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/account-layout/account-layout.module#AccountLayoutModule'
            }
        ]
    }

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {
            useHash: true
        })
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
