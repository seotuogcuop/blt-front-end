import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../model/item.model';
import { User } from '../model/user.model';
import { environment} from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
const URL = environment.API_URL;
@Injectable({
  providedIn: 'root'
})
export class UserService {

  status: string[] = ['OUTOFSTOCK', 'INSTOCK', 'LOWSTOCK'];

  
  userNames: string[] = [
      "Bamboo Watch", 
      "Black Watch", 
      "Blue Band", 
      "Blue T-Shirt", 
      "Bracelet", 
      "Brown Purse", 
      "Chakra Bracelet",
      "Galaxy Earrings",
      "Game Controller",
      "Gaming Set",
      "Gold Phone Case",
      "Green Earbuds",
      "Green T-Shirt",
      "Grey T-Shirt",
      "Headphones",
      "Light Green T-Shirt",
      "Lime Band",
      "Mini Speakers",
      "Painted Phone Case",
      "Pink Band",
      "Pink Purse",
      "Purple Band",
      "Purple Gemstone Necklace",
      "Purple T-Shirt",
      "Shoes",
      "Sneakers",
      "Teal T-Shirt",
      "Yellow Earbuds",
      "Yoga Mat",
      "Yoga Set",
  ];
    userSubject: any;
    currentUser: any;
  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  regiserAccount (account: any): Observable<any> {
    return this.http.post<any>(URL + 'api/v1/user/register', account,  this.httpOptions).pipe(
        tap((newAccount: any) => console.log('add')),
        catchError(this.handleError())
    );
  }
  login (display_name: any, password: any): Observable<any> {
      const params: any = {
        username: display_name,
        password: password
      }
    return this.http.post<any>(URL + 'login', params).pipe(
        tap((newUser: any) => {
            this.userSubject = newUser;
        }
        
        ),
        catchError(this.handleError<any>('login')));
  }
  public get userValue(): User {
    return this.userSubject.value;
}
  getUsers() {
      return this.http.get<any>('assets/users.json')
      .toPromise().then(response => <Item[]>response.data).then(data => { console.log('data', data); return data; });
  }

  setCurrentUser (response: any) {
      this.currentUser = response;

      return this.currentUser;
  }
  checkLogin(): Observable<any> {
    console.log('checkLogin');
    // if (environment.env === 'development') {
    //     return new Observable(observer => {
    //         observer.next(this.developmentUser);
    //     });
    // } else {
    return new Observable(observer => {
        this.http.get(URL + 'login').subscribe(data => {
            return observer.next(data);
        },
        error => {
            return observer.next(error);
        });
    });
    // }
}
  generateUsers(): User {
      const users: User =  {
          id: this.generateId(),
          username: this.generateName(),
          full_name: this.generateName(),
          
      };

      // product.image = product.name.toLocaleLowerCase().split(/[ ,]+/).join('-')+".jpg";;
      return users;
  }

  generateId() {
      let text = "";
      let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      
      for (var i = 0; i < 5; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      
      return text;
  }

  generateName() {
      return this.userNames[Math.floor(Math.random() * Math.floor(30))];
  }

  generatePrice() {
      return Math.floor(Math.random() * Math.floor(299)+1);
  }

  generateQuantity() {
      return Math.floor(Math.random() * Math.floor(75)+1);
  }

  generateStatus() {
      return this.status[Math.floor(Math.random() * Math.floor(3))];
  }

  generateRating() {
   
    return Math.floor(Math.random() * Math.floor(5)+1);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instea
      return of(result as T);
    };
  }
}
