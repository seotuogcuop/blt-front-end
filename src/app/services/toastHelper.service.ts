import { MessageService } from 'primeng/api';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ToastHelperService {
    constructor(
        private messageService: MessageService
    ) { }
    addErrorMessage(
        message: string = '',
        summary: string = 'Lỗi',
        key: string = 'toast') {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'error', summary: summary, detail: message
        });
    }
    addErrorCheckMessage (
      message: string = '',
      summary: string = 'Lỗi',
      key: string = 'toast') {
      setTimeout(() => {
          this.messageService.clear();
      }, 3000);
      this.messageService.add({
          key: key, severity: 'error', summary: summary, detail: message
      });
  }
    addSuccessMessage(
        message: string = '',
        summary: string = 'Thành công',
        key: string = 'toast') {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'success', summary: summary, detail: message
        });
    }
    addSuccessMessageBottom(
        message: string = '',
        summary: string = 'Thành công',
        key: string = 'br'
    ) {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'info', summary: summary, detail: message
        });
    }
    addInfoMessage(
        message: string = '',
        summary: string = 'Thông tin',
        key: string = 'toast') {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'info', summary: summary, detail: message
        });
    }
    addWarnMessage(
        message: string = '',
        summary: string = 'Cảnh báo',
        key: string = 'toast') {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'warn', summary: summary, detail: message
        });
    }
    addCustomMessage(
        message: string = '',
        summary: string,
        key: string = 'toast') {
        setTimeout(() => {
            this.messageService.clear();
        }, 1000);
        this.messageService.add({
            key: key, severity: 'custom', summary: summary, detail: message
        });
    }
}
