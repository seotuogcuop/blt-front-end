import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    private showSpinner = new BehaviorSubject<boolean>(false)

    constructor() { }

    returnAsObservable = () => {
        return this.showSpinner.asObservable();
    }

    show = () => {
        this.showSpinner.next(true);
    }

    hide = () => {
        this.showSpinner.next(false);
    }
}
