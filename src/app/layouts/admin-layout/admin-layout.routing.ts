import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { ItemsComponent } from '../../pages/items/items.component';
import { UsersComponent } from 'src/app/pages/users/users.component';
import { CategoryComponent } from 'src/app/pages/category/category.component';
import { PostComponent } from 'src/app/pages/post/post.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'items/collection', component: ItemsComponent},
    { path: 'users/collection', component: UsersComponent},
    { path: 'categories/collection',component: CategoryComponent},
    { path: 'posts/collection', component: PostComponent}
];
