import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';

import {  } from './account-layout.routing';
import { AccountLayoutComponent } from './account-layout.component';
import { LogginComponent } from '../../pages/loggin/loggin.component';
import { RegisterComponent } from '../../pages/register/register.component';
@NgModule({
  declarations: [
  //  LogginComponent,
  //    RegisterComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AccountLayoutModule { }
