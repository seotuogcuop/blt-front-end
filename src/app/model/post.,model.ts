export class Post {
    id?:string;
    title?:string;
    shortDescription?:string;
    longDescription?:string;
}