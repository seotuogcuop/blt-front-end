export class Item {
    id?:string;
    code?:string;
    name?:string;
    description?:string;
    acreage?: number;
    price?:number;
    quantity?:number;
    inventoryStatus?:string;
    address?: string;
    category?:string;
    image?:string;
    rating?:number;
}