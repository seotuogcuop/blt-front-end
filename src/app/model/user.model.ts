export class User {
    id?:string;
    username?:string;
    password?:string;
    full_name?:string;
    email?:string;
    address?:string;
    mobile?:any;
    firstName?: string;
    lastName?: string;
    token?: string;


}