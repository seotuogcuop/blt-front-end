import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { AccounntService } from '../services/accounnt.service';
import { UserService } from '../services/users.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userService: UserService
    ) {
        console.log('run on here ==============>')
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        console.log('current')
        const user = this.userService.userValue;
        return new Observable(observer => {
            const continueURL = encodeURIComponent('https://' + location.host + '/#' + state.url);
            this.userService.checkLogin().subscribe(data => {
                if (data && data.error) {
                    this.router.navigateByUrl('login');
                    observer.next(false);
                } else if (!data || data.error_message || data.error_code) {
                    this.router.navigateByUrl('login');
                    observer.next(false);
                } else {
                    console.log('data', data);
                    this.userService.setCurrentUser(data.user);
                    this.router.navigateByUrl('dashboard');
                    
                    observer.next(true);
                    
                }
            });
        });
        // console.log('current_users', user);
        // if (user) {
        //     console.log('user', user);
        //     this.router.navigateByUrl('dashbroad');
        //     return true;
        // }
        // // not logged in so redirect to login page with the return url

        // this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
        // return false;
    }
}