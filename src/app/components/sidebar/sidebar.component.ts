import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'icon-dashboard text-danger', class: '' },
  { path: '/items/collection', title: 'Sản phẩm', icon: 'icon-archive text-primary', class: '' },
  { path: '/users/collection', title: 'Người dùng', icon: 'icon-user text-primary', class: ''},
  { path: '/categories/collection', title: 'Danh mục đất', icon: 'icon-sitemap text-danger', class: ''},
  { path: '/posts/collection', title: 'Bài đăng', icon: 'icon-rss-sign text-warning', class: ''}
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }
}
